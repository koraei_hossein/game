package com.medric.game.controller;

import com.medric.game.dto.Response;
import com.medric.game.exceptions.InvalidInputException;
import com.medric.game.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<Response<String>> handleNotFoundException(
            NotFoundException ex) {

        return new ResponseEntity<>(new Response<>("not_found", ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<Response<String>> handleInvalidInputException(
            InvalidInputException ex) {

        return new ResponseEntity<>(new Response<>("invalid_score", ex.getMessage()), HttpStatus.BAD_REQUEST);
    }
}

package com.medric.game.controller;


import com.medric.game.dto.LeaderBoardDto;
import com.medric.game.dto.UserDto;
import com.medric.game.exceptions.InvalidInputException;
import com.medric.game.exceptions.NotFoundException;
import com.medric.game.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Logger;

@RestController
@RequestMapping("game")
public class UserController {

    @Autowired
    UserService userService;

    private static final Logger LOG = Logger.getLogger(UserService.class.getName());

    public UserController() {
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(value = "/score", produces = {"application/json; charset=UTF-8"})
    public ResponseEntity<UserDto> updateScore(@RequestBody UserDto userDto) throws NotFoundException, InvalidInputException {
        logUrl("/score");
        return new ResponseEntity<>(userService.saveUser(userDto), HttpStatus.OK);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(value = "/score")
    public ResponseEntity<LeaderBoardDto> leaderBoard(@RequestParam long playerId) throws NotFoundException {
        logUrl("/score?playerId=" + playerId);
        return new ResponseEntity<>(userService.LeaderBoard(playerId), HttpStatus.OK);
    }

    void logUrl(String message) {
        LOG.info("URL : game" + message);
    }
}

package com.medric.game.dao;

import com.medric.game.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends JpaRepository<User, Long> {

    List<User> findTop100ByOrderByScoreDesc();

    @Query(value = "SELECT tbl_rank.rank FROM (SELECT ROW_Number() OVER (ORDER BY c_score DESC) AS rank" +
            ", c_id, c_score FROM tbl_user) AS tbl_rank " +
            " WHERE tbl_rank.c_id = :id"
            , nativeQuery = true)
    int findUserRankById(@Param("id") long id);
}

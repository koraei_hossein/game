package com.medric.game.dto;

public class UserDto {

    private long playerId;
    private int score;
    private Long datetime;

    public UserDto(long playerId, int score, Long datetime) {
        this.playerId = playerId;
        this.score = score;
        this.datetime = datetime;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Long getDatetime() {
        return datetime;
    }

    public void setDatetime(Long datetime) {
        this.datetime = datetime;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "playerId=" + playerId +
                ", score=" + score +
                ", timestamp=" + datetime +
                '}';
    }
}

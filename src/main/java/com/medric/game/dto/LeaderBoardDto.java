package com.medric.game.dto;

import java.util.List;

public class LeaderBoardDto {

    private long playerId;
    private long score;
    private int rank;
    private List<UserBoardDto> records;

    public LeaderBoardDto() {
    }

    public LeaderBoardDto(long playerId, long score, int rank, List<UserBoardDto> records) {
        this.playerId = playerId;
        this.score = score;
        this.rank = rank;
        this.records = records;
    }

    public List<UserBoardDto> getRecords() {
        return records;
    }

    public void setRecords(List<UserBoardDto> records) {
        this.records = records;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}

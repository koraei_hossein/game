package com.medric.game.dto;

import com.medric.game.entity.User;

public class UserBoardDto {

    private long playerId;
    private long score;
    private int rank;

    public UserBoardDto(long playerId, long score, int rank) {
        this.playerId = playerId;
        this.score = score;
        this.rank = rank;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public static UserBoardDto create(User user){
        return new UserBoardDto(user.getId(), user.getScore(), 0);
    }
}

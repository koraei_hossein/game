package com.medric.game.dto;

public class Response<T> {

    private String msg;
    private T data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Response(String msg, T data) {
        this.msg = msg;
        this.data = data;
    }

    public Response(T data) {
        this.data = data;
    }

    public Response(String msg) {
        this.msg = msg;
    }
}

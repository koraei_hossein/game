package com.medric.game.service;

import com.medric.game.dao.UserDao;
import com.medric.game.dto.LeaderBoardDto;
import com.medric.game.dto.UserBoardDto;
import com.medric.game.dto.UserDto;
import com.medric.game.entity.User;
import com.medric.game.exceptions.InvalidInputException;
import com.medric.game.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class UserService {

    @Autowired
    UserDao userDao;

    public UserService() {
    }

    public UserDto saveUser(UserDto userDto) throws InvalidInputException {

        if (!scoreIsValidate(userDto.getScore()))
            throw new InvalidInputException(String.valueOf(userDto.getScore()));
        User user = new User();
        user.setId(userDto.getPlayerId());
        user.setScore(userDto.getScore());
        userDto.setDatetime(new Timestamp(System.currentTimeMillis()).getTime());
        userDao.save(user);
        return userDto;
    }

    public LeaderBoardDto LeaderBoard(long id) throws NotFoundException {
        if (!isExist(id))
            throw new NotFoundException(String.valueOf(id));

        LeaderBoardDto leaderBoardDto = new LeaderBoardDto();

        List<User> users = userDao.findTop100ByOrderByScoreDesc();
        List<UserBoardDto> userBoardDtoList = users.stream().map(UserBoardDto::create).collect(Collectors.toList());

        IntStream.range(0, userBoardDtoList.size()).forEach(i -> {
            UserBoardDto userBoardDto = userBoardDtoList.get(i);
            userBoardDto.setRank(i + 1);
            userBoardDtoList.set(i, userBoardDto);
        });

        leaderBoardDto.setRank(userDao.findUserRankById(id));
        leaderBoardDto.setScore(userDao.findById(id).get().getScore());
        leaderBoardDto.setPlayerId(id);
        leaderBoardDto.setRecords(userBoardDtoList);
        return leaderBoardDto;
    }

    boolean isExist(long id) {
        return userDao.findById(id).isPresent();
    }

    boolean scoreIsValidate(int score) {
        return score <= 100 && score >= 0;
    }
}

package com.medric.game.exceptions;

public class NotFoundException extends Exception {
    public NotFoundException(String id) {
        super(id);
    }
}
